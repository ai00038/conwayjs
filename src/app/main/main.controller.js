(function (){
	'use strict';
	
	angular
		.module('main')
		.controller('MainController', MainController);
			
	function MainController($interval) {
	
		var vm = this;
		
		vm.arr = [ 
			[0,1,1,0,1,0,1,0,1,0],
			[0,1,0,0,1,1,1,0,1,0],
			[1,1,0,0,0,1,1,0,1,0],
			[0,0,1,1,1,0,1,0,1,0],
			[0,0,1,1,0,1,1,0,1,0],
			[0,1,0,0,1,1,1,0,1,0],
			[1,1,0,0,0,1,1,0,1,0],
			[0,0,1,1,1,0,1,0,1,0],
			[0,0,1,1,0,1,1,0,1,0],
			[1,1,1,0,0,0,1,0,1,0]
		];
		
		vm.modifyGrid = modifyGrid;
		
		$interval(activate, 250);
		
		function activate(){
			vm.arr = calculateNextGeneration(vm.arr);
		}
		
		function modifyGrid(customArr){
			vm.arr = customArr;
		}
		
		function createEmptyGrid(rows, cols){
			var empty = [];
			for(var i = 0; i < rows; i++){
				empty.push([]);
				for(var j = 0; j < cols; j++){
					empty[i].push(null);
				}
			}
		  return empty;
		}

		function calculateNextGeneration(arr){
			var ROWS = arr.length,
				COLS = arr[0].length,
			  nextGenArr = createEmptyGrid(ROWS, COLS);

			for(var i = 0; i < ROWS; i++){
				for(var j = 0; j < COLS; j++){
					(function (){
						var liveNeighbours = 0;
						
						if(i < ROWS - 1){
							liveNeighbours += arr[i+1][j];
						}
						
						if(i > 0){
							liveNeighbours += arr[i-1][j];
						}
						
						if(j < COLS - 1){
							liveNeighbours += arr[i][j+1];
						}
						
						if(j > 0){
							liveNeighbours += arr[i][j-1];
						}
						
						if( (i < ROWS - 1) && (j < COLS - 1) ){
							liveNeighbours += arr[i+1][j+1];
						}
						
						if( (i < ROWS - 1) && (j > 0) ){
							liveNeighbours += arr[i+1][j-1];
						}
						
						if( (i > 0) && (j < COLS - 1) ){
							liveNeighbours += arr[i-1][j+1];
						}
						
						if( (i > 0) && (j > 0) ){
							liveNeighbours += arr[i-1][j-1];
						}
						
						//by default, let i,j entry of next gen cell be the i,j entry of prev gen cell. 
						nextGenArr[i][j] = arr[i][j];
						
						//live neighbours all counted - determine fate of cell otherwise
						
						if(arr[i][j] === 1){//if alive
							if(liveNeighbours < 2 || liveNeighbours > 3){
								nextGenArr[i][j] = 0;
							}
						} else {//if dead
							if(liveNeighbours === 3){
								nextGenArr[i][j] = 1; // becomes alive
							}
						}
				
			  })();
			}
		  }

			return nextGenArr;
		}
	}
	
})();